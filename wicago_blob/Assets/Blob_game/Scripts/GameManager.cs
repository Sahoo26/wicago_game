﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using Com.Wigaco;


//using NDream.AirConsole;
//using Newtonsoft.Json.Linq;

public class GameManager : MonoBehaviour {

    [DllImport("__Internal")]
    private static extern void Hello();

    [DllImport("__Internal")]
    private static extern void OnSceneReady();

    [DllImport("__Internal")]
    private static extern void OnGameReady();

    [DllImport("__Internal")]
    private static extern void OnGameStart();

    [DllImport("__Internal")]
    private static extern void OnShowLeaderboard();

    [DllImport("__Internal")]
    private static extern void OnGameEnd();

    ////Definition of Player Objects
    List<string> Blobs = new List<string>();
    public GameObject[] BLOB;
    public GameObject Collectable;
    float GameTimer = 60.0f;
    public GameObject Instructionspanel,countpanel;
    public TMPro.TextMeshProUGUI TimerLabelValue;

    
#if !DISABLE_AIRCONSOLE 

    public void ExamplePongLogic()
    {
        Console.WriteLine("External Ojbect constructor");
    }

    void Start()
    {

       // OnGameStart();
        Debug.Log("Unity:: ExamplePongLogic.Start()");
        Console.Write("Unity Start()");
        Wigaco a = Wigaco.Instance;
    }

    void Awake () {
       // onGameReady();
        Console.Write("Unity Awake()");
       // Wigaco.Instance.OnMessage += _cntrl;
    }

    // called first
    void OnEnable()
    {
        Console.Write("Unity OnEnable() called");
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Console.Write("Unity OnSceneLoaded(): " + scene.name);
    }

    public void Conncetplayers(int no)
    {
        for(int i = 0; i < no; i++)
        {
            OnConnect(i.ToString());
        }
    }
   
    IEnumerator  _ShowInstructions()
    {
        countpanel.SetActive(false);
        Instructionspanel.SetActive(true);
        yield return new WaitForSeconds(5f);
        Instructionspanel.SetActive(false);
        StartGame();
    }

    private List<GameObject> collectablesSpawned = new List<GameObject>();
    public void _spawnCollectable()
    {
        GameObject item = Instantiate(Collectable, this.transform);
        collectablesSpawned.Add(item);
        Debug.Log(collectablesSpawned[0].GetComponent<Collectable>()._itype.ToString());
    }


    IEnumerator DespawnCollcetable()
    {
        if (collectablesSpawned.Count > 0)
        {
            foreach (GameObject t in collectablesSpawned)
            {
                Destroy(t);
            }
            collectablesSpawned = new List<GameObject>();
        }
        yield return new WaitForSeconds(0f);
        _spawnCollectable();
    }

    public void _playerAction(GameObject CollectedObj,int playerno)
    {
        if (BLOB[playerno].GetComponent<Blob_player>()._isinIdlestate()) {
            int valuetoadd = CollectedObj.GetComponent<Collectable>()._collectableValue;
            if (valuetoadd < 1)
            {
                //playerbombhitanimation

            }
            else
            {
                BLOB[playerno].GetComponent<Blob_player>().Catch_collectable(valuetoadd);
            }
            StartCoroutine(DespawnCollcetable());
        }
    }

    public void OnConnect(string socketId)
    {
        Blobs.Add(socketId);
        int idno = int.Parse(socketId);
        BLOB[idno].SetActive(true);
        BLOB[idno].GetComponent<Blob_player>().SetPlayer(idno);
        Console.Write("Players Count: ");
        Console.WriteLine(Blobs.Count);

          if (Blobs.Count >= 2) {
            StartCoroutine(_ShowInstructions());
          } else {
              Console.Write("NEED MORE PLAYERS");
          }
    }

    // wigaco
    public void _cntrl(string text)
    {
        print(Blobs.Count);
        string[] data = text.Split(' ');
        int active_player = Blobs.IndexOf(data[0]);
        if (active_player != -1)
        {
           // float move = Convert.ToSingle(data[1]);
            Console.Write("Catch From Unity: ");
            Console.WriteLine("----------------");
            // BlobCatch(active_player);
            if (collectablesSpawned.Count > 0)
            {
                _playerAction(collectablesSpawned[0], active_player);
            }
        }
    }




    // wigaco
    public void resetPlayers()
    {
        Blobs = new List<string>();
    }

    /// <summary>
    /// We start the game if 2 players are connected and the game is not already running (activePlayers == null).
    /// 
    /// NOTE: We store the controller device_ids of the active players. We do not hardcode player device_ids 1 and 2,
    ///       because the two controllers that are connected can have other device_ids e.g. 3 and 7.
    ///       For more information read: http://developers.airconsole.com/#/guides/device_ids_and_states
    /// 
    /// </summary>
    /// <param name="device_id">The device_id that connected</param>
   

    // wigaco
    void OnDisconnect(string socketId)
    {
        Blobs.Remove(socketId);
        int idno = int.Parse(socketId);
        BLOB[idno].SetActive(false);

        Console.Write("Once Player Disconnected: Current Players Count: ");
        Console.WriteLine(Blobs.Count);

        if (Blobs.Count >= 2)
        {
            Console.Write("ONE PLAYER DISCONNECTED BUT GAME CONTINUES");
        }
        else
        {
            Console.Write("NEED MORE PLAYERS TO START GAME");
        }
    }

    

    void StartGame() {
        //OnGameStart();
        _gameStart = true;
        _spawnCollectable();
    }

    bool _gameStart=false;
     void Update( )
     {
        /////////////////////////////
        ///test case keycodes
        if (BLOB[0].activeInHierarchy && Input.GetKeyDown(KeyCode.Alpha1))
        {
            _cntrl("0");
        }

        if (BLOB[1].activeInHierarchy && Input.GetKeyDown(KeyCode.Alpha2))
        {
            _cntrl("1");
        }

        if (BLOB[2].activeInHierarchy && Input.GetKeyDown(KeyCode.Alpha3))
        {
            _cntrl("2");
        }

        if (BLOB[3].activeInHierarchy && Input.GetKeyDown(KeyCode.Alpha4))
        {
            _cntrl("3");
        }
        /////////////////////////
        ///

        if (_gameStart)
        {
            GameTimer -= Time.deltaTime;
            TimerLabelValue.text = ((int)GameTimer).ToString();
            if (GameTimer <= 0)
            {
                //Cannon ball bomb explodes
                GameTimer = 0f;
                //OnGameEnd();
                resetPlayers();
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
     }


    void OnDestroy () {
        // unregister wicago events on scene change
    }
#endif
}
