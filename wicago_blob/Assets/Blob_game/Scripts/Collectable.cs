﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public enum Collectabletype
	{
	    Bomb = 0,
	    Apple = 1,
	    watermelon = 2,
	    Orange = 3,
	    Gauva =4,
	    Strawberry =5,
	    Burger = 6
	}

	public Sprite[] _CollectableSprites;
	public Collectabletype _itype = Collectabletype.Apple;
	public int _collectableValue = 1;

	static T GetRandomEnum<T>()
	{
	    System.Array A = System.Enum.GetValues(typeof(T));
	    int rndno = UnityEngine.Random.Range(0,A.Length);
	    T V = (T)A.GetValue(rndno);
	    return V;
	}

    void Start()
    {
    	_itype = GetRandomEnum<Collectabletype>();
    	Debug.Log(_itype.ToString());
        transform.GetComponent<SpriteRenderer>().enabled = true;
        transform.GetComponent<SpriteRenderer>().sprite = _CollectableSprites[(int)_itype];
	    _collectableValue =(int)_itype;
    }

}
