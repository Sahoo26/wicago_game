﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public enum PlayerState
{
    Live,
    Disconnected
}

public class Blob_player : MonoBehaviour
{
	#region Public_Variables
	public float _myScore=0;
	public float PlayerID=0;
    public TextMeshPro _myScoretxt;
	#endregion

    private PlayerState playerState = PlayerState.Disconnected;

	Animator playerAnimContoller;

    // ON Button Click
    public void Catch_collectable(int scorevalue)
    {
        if(playerState == PlayerState.Live){
            //Do catch animation & add score
            playerAnimContoller.SetBool("BombHit", false);
            playerAnimContoller.SetBool("Collect", true);
            _myScore +=scorevalue;
            //Debug.Log("***************" + PlayerID + "**" + _myScore);
            _myScoretxt.text = _myScore.ToString();
        }
    }

    public bool _isinIdlestate()
    {
        return playerAnimContoller.GetCurrentAnimatorStateInfo(0).IsName("BlobIdle");
    }

    public void _toidle()
    {
        _myScoretxt.text = _myScore.ToString();
        playerAnimContoller.SetBool("BombHit", false);
        playerAnimContoller.SetBool("Collect", false);
    }

   public void _playBombhitAnimation()
    {
        playerAnimContoller.SetBool("BombHit", true);
        playerAnimContoller.SetBool("Collect", false);
    }

    ///Intialising Player Object
    public void SetPlayer(int playerid){
        _myScore = 0;
		PlayerID = playerid;
        playerState = PlayerState.Live;
        playerAnimContoller = this.transform.GetComponent<Animator>();
	}

    //public static Blob_player Instance ;

    public PlayerState PlayerState
    {
        get
        {
            return playerState;
        }

        private set
        {
            if (value != playerState)
            {
                value = playerState;
                PlayerStateChanged(playerState);
            }
        }
    }

    void OnEnable()
    {
        PlayerStateChanged(PlayerState.Live);
    }

    void OnDisable()
    {
        PlayerStateChanged(PlayerState.Disconnected);
    }

    private void PlayerStateChanged(PlayerState obj)
    {
        if (obj == PlayerState.Live)
        {
            PlayerLive();
        }
        else if (obj == PlayerState.Disconnected)
        {
            PlayerDisconnected();
        }
    }



    /*void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(Instance.gameObject);
            Instance = this;
        }
    }

    void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }
    }*/




    void PlayerLive()
    {
        //Fire event
        //PlayerState = PlayerState.Live;//Player Idle Animation
    }

    void PlayerDisconnected()
    {
        //Fire event
       // PlayerState = PlayerState.Disconnected;//Player Left Animation
    }

    public void PlayerCollectItem(){
        ///collectitem
    }
    
}
