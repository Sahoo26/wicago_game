﻿using UnityEngine;
using System.Collections;

namespace Com.Wigaco
{
    public delegate void OnMessageDelegate(string msg);

    public class Wigaco : MonoBehaviour
    {
        public event OnMessageDelegate OnMessage;

        private static Wigaco _instance;

        public static Wigaco Instance {
            get {
                Debug.Log("Unity:: WigacoInstance(): Wigaco");
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<Wigaco>();
                    if (_instance != null)
                    {
                        DontDestroyOnLoad(_instance.gameObject);
                    }
                }

                return _instance;
            }
        }

        void OnBecameVisible()
        {
            Debug.Log("Unity:: OnBecameVisible(): Wigaco");
        }

        void OnMessageDelegate(string msg)
        {
            if (this.OnMessage != null)
            {
                this.OnMessage(msg);
            }

        }
    }

}