mergeInto(LibraryManager.library, {

  Hello: function () {
    // window.alert("Hello, world!");
    // unityFn.globalFn();
  },

  OnGameStart: function() {
    GameManager.onGameStart();
  },

  OnSceneReady: function() {
    GameManager.OnSceneReady();
  },

  OnShowLeaderboard: function() {
    GameManager.OnShowLeaderboard();
  },
  
  OnGameEnd: function() {
    GameManager.onGameEnd();
  },
  
  onGameReady: function() {
    GameManager.onGameReady();
  }

});